#pragma checksum "D:\ТГУ\7\site-asp-net\WebApplication\WebApplication\Views\Home\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fad994d024e32a5a2322ab0570c7619e7d0b8159"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_Index), @"mvc.1.0.view", @"/Views/Home/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "D:\ТГУ\7\site-asp-net\WebApplication\WebApplication\Views\_ViewImports.cshtml"
using WebApplication;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "D:\ТГУ\7\site-asp-net\WebApplication\WebApplication\Views\_ViewImports.cshtml"
using WebApplication.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"fad994d024e32a5a2322ab0570c7619e7d0b8159", @"/Views/Home/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"c58edd3a6b5e9ca63b10fbb3cbb99bbeb61e4bcd", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "D:\ТГУ\7\site-asp-net\WebApplication\WebApplication\Views\Home\Index.cshtml"
  
    ViewData["Title"] = "Home Page";

#line default
#line hidden
#nullable disable
            WriteLiteral(@"
<section class=""std block"">
    <div class=""grid-block"">
        <div class=""std__content"">
            <h1>Статьи по разработке на React Native.</h1>
            <p>React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.</p>
            <p>
                Use a little—or a lot. You can use React Native today in your existing Android and iOS projects or you can create a whole new app from scratch.
            </p>
        </div>

        <div class=""phone"">
            <div class=""phone__camera""></div>
            <div class=""phone__song""></div>
            <div class=""phone__screen"">
                <svg class=""react"" xmlns=""http://www.w3.org/2000/svg"" width=""64"" height=""64"" viewBox=""0 0 32 32"">
                    <g transform=""matrix(.05696 0 0 .05696 .647744 2.43826)"" fill=""none"" fill-rule=""evenodd"">
                        <circle r=""50.167"" cy=""237.628"" cx=""269.529"" fill=""#00d8ff""/>
                      ");
            WriteLiteral(@"  <g stroke=""#00d8ff"" stroke-width=""24"">
                            <path d=""M269.53 135.628c67.356 0 129.928 9.665 177.107 25.907 56.844 19.57 91.794 49.233 91.794 76.093 0 27.99-37.04 59.503-98.083 79.728-46.15 15.29-106.88 23.272-170.818 23.272-65.554 0-127.63-7.492-174.3-23.44-59.046-20.182-94.61-52.103-94.61-79.56 0-26.642 33.37-56.076 89.415-75.616 47.355-16.51 111.472-26.384 179.486-26.384z""/><path d=""M180.736 186.922c33.65-58.348 73.28-107.724 110.92-140.48C337.006 6.976 380.163-8.48 403.43 4.937c24.248 13.983 33.042 61.814 20.067 124.796-9.8 47.618-33.234 104.212-65.176 159.6-32.75 56.788-70.25 106.82-107.377 139.272-46.98 41.068-92.4 55.93-116.185 42.213-23.08-13.3-31.906-56.92-20.834-115.233 9.355-49.27 32.832-109.745 66.8-168.664z""/><path d=""M180.82 289.482C147.075 231.2 124.1 172.195 114.51 123.227c-11.544-59-3.382-104.11 19.864-117.566 24.224-14.024 70.055 2.244 118.14 44.94 36.356 32.28 73.688 80.837 105.723 136.173 32.844 56.733 57.46 114.21 67.036 162.582 12.117 61.213 2.31 107.984-21.453 1");
            WriteLiteral(@"21.74-23.057 13.348-65.25-.784-110.24-39.5-38.013-32.71-78.682-83.253-112.76-142.115z""/>
                        </g>
                    </g>
                </svg>
                <div class=""phone__content"">
                    Hello World
                </div>
            </div>
            <div class=""phone__button""></div>
        </div>

    </div>
</section>

<section class=""articles block"">
    <div class=""grid-block"">

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install ");
            WriteLiteral(@"and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</");
            WriteLiteral(@"div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>

        <a href=""/qwe"" class=""article"">
            <div class=""article__inner"">
                <h2 class=""article__title"">Setting up the development environment</h2>
                <div class=""article__note"">This page will help you install and build your first React Native app.</div>
            </div>
        </a>
");
            WriteLiteral("\r\n\r\n    </div>\r\n</section>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
